﻿using Generated;
using Grpc.Core;
using System;
using System.Globalization;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            string dateRead;
            DateTime date;


            do
            {
                Console.WriteLine("Enter date: ");
                dateRead = Console.ReadLine();

                if (isValidDate(dateRead, out date))
                {
                    var season = getSeason(date);
                    switch(season)
                    {
                        case 1:
                            {
                                var client = new WinterService.WinterServiceClient(channel);
                                var response = client.sendZodiacSign(new WinterRequest() { Date = dateRead });
                                Console.WriteLine("This one is in winter!");
                                Console.WriteLine("Client received response: {0}", response.ZodiacSign);
                                break;
                            }

                        case 2:
                            {
                                var client = new SpringService.SpringServiceClient(channel);
                                var response = client.sendZodiacSign(new SpringRequest() { Date = dateRead });
                                Console.WriteLine("This one is in spring!");
                                Console.WriteLine("Client received response: {0}", response.ZodiacSign);
                                break;
                            }

                        case 3:
                            {
                                var client = new SummerService.SummerServiceClient(channel);
                                var response = client.sendZodiacSign(new SummerRequest() { Date = dateRead }); ;
                                Console.WriteLine("This one is in summer!");
                                Console.WriteLine("Client received response: {0}", response.ZodiacSign);
                                break;
                            }

                        case 4:
                            {
                                var client = new AutumnService.AutumnServiceClient(channel);
                                var response = client.sendZodiacSign(new AutumnRequest() { Date = dateRead });
                                Console.WriteLine("This one is in autumn!");
                                Console.WriteLine("Client received response: {0}", response.ZodiacSign);
                                break;
                            }
                    }
                }
                else
                {
                    writeError("ERR! The date introduced is not valid!");
                    writeInfo("INFO! The date needs to contain only '/' symbol, no spaces and has to be in mm/dd/yyyy format!");
                }
            } while (!isValidDate(dateRead, out date));

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
        private static bool isValidDate(string date, out DateTime result)
        {
            CultureInfo enUS = new CultureInfo("en-US");
            if (DateTime.TryParseExact(date, "MM/dd/yyyy", enUS, DateTimeStyles.None, out result))
            {
                return true;
            }
            else { return false; }
        }

        private static void writeError(string errorMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(errorMessage.PadRight(Console.WindowWidth - 1));
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void writeInfo(string info)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(info.PadRight(Console.WindowWidth - 1));
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static int getSeason(DateTime date)
        {
            if(date.Month == 12 || date.Month == 1 || date.Month == 2)
            {
                return 1; //Winter
            }

            if (date.Month >= 3 && date.Month <= 5)
            {
                return 2; //Spring
            }
            
            if (date.Month >= 6 && date.Month <= 8)
            {
                return 3; //Summer
            }
            
            if (date.Month >= 9 && date.Month <= 11)
            {
                return 4; //Autumn
            }

            return 0;
        }
    }
}
