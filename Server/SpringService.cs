﻿using Generated;
using Grpc.Core;
using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace Server
{
    internal class SpringService : Generated.SpringService.SpringServiceBase
    {
        public override Task<ZodiacResponse> sendZodiacSign(SpringRequest request, ServerCallContext context)
        {
            CultureInfo enUS = new CultureInfo("en-US");
            DateTime date;
            DateTime.TryParseExact(request.Date, "MM/dd/yyyy", enUS, DateTimeStyles.None, out date);
            String zodiacSign = "";

            try
            {
                using (StreamReader sr = new StreamReader(@"..\..\..\spring.txt"))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (getZodiacSign(date, line, out zodiacSign))
                        {
                            Console.WriteLine(zodiacSign);
                            break;
                        }

                    }
                }
            }
            catch (IOException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERR! The file could not be read:");
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }

            return Task.FromResult(new ZodiacResponse() { ZodiacSign = zodiacSign });
        }

        private static bool getZodiacSign(DateTime date, String line, out String zodiacSign)
        {
            string[] infos;
            infos = line.Split(' ');
            zodiacSign = infos[0];
            DateTime startDate = DateTime.Parse(infos[1]);
            DateTime endDate = DateTime.Parse(infos[2]);

            if (date.Month == startDate.Month && date.Month == endDate.Month)
            {
                if (date.Day >= startDate.Day && date.Day <= endDate.Day)
                {
                    return true;
                }
            }

            if(startDate.Month != endDate.Month)
            {
                if (date.Month >= startDate.Month && date.Month <= endDate.Month)
                {
                    if ((date.Month == startDate.Month && date.Day >= startDate.Day) || (date.Month == endDate.Month && date.Day <= endDate.Day))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
